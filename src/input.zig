const std = @import("std");
const READ_BUFFER = 1024;

pub var stdin: std.fs.File = undefined;
pub var allocator: std.mem.Allocator = undefined;

pub var delimiter: u8 = '\n'

/// reads line?
pub fn readLine() !?[]const u8 {
    var buffer = try allocator.alloc(u8, READ_BUFFER);
    var read_count = stdin.read(buffer);
    if (read_count == buffer.len) return error.StreamTooLong;
    
    var index: usize = 0;

    for (buffer) |char, delimiter_index| {
        if (char == delimiter) {
            index = delimiter_index;
            break;
        }
        continue;
    }

    if (index == 1) return null;
    
    
}
