const std = @import("std");
const config = @import("config.zig");
const input = @import("input.zig");
const output = @import("output.zig");
const client = @import("client.zig");

const zirc_version = "beta";

const Profile = struct {
    username: []const u8,
    hostname: []const u8,
    port: u16,
    protocol: Protocol,
};

const Protocol = enum {
    none,
    irc,
    irctls,
};

pub fn main() !void {

    // initialize allocator
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer {
        const leaked = gpa.deinit();
        if (leaked) @panic("\nLeaked memory, panicking...");
    }

    _ = allocator;

    input.stdin = std.io.getStdIn();
    output.stdout = std.io.getStdOut();
    output.stderr = std.io.getStdErr();

    try output.prettyPrint(.info, "Zirc {s}", .{zirc_version});
    var profile = try config.getConfig();
}
