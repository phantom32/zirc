const std = @import("std");

pub var stdout: std.fs.File = undefined;
pub var stderr: std.fs.File = undefined;

pub const OutputMode = enum {
    greet,
    info,
    warn,
    err,
};

const Colors = struct {
    pub const RESET = "\x1b[0m";
    pub const RED = "\x1b[31m";
    pub const GREEN = "\x1b[32m";
    pub const YELLOW = "\x1b[33m";
    pub const BLUE = "\x1b[34m";
    pub const MAGENTA = "\x1b[35m";
    pub const CYAN = "\x1b[36m";
    pub const INVERSE = "\x1b[7m";
    pub const BLINK = "\x1b[5m";
};

pub fn prettyPrint(mode: OutputMode, comptime fmt: anytype, msg: anytype) !void { // printing pretty, for zirc, prints trusted text to terminal
    const writer = stdout.writer();
    switch (mode) {
        // print without any formatting
        .greet => {
            try writer.print("{s}", .{Colors.RESET});
            try writer.print(fmt, msg);
            try writer.print("\n", .{});
        },
        // info print with colors
        .info => {
            try writer.print("{s}[INFO]{s} ", .{ Colors.BLUE, Colors.RESET });
            try writer.print(fmt, msg);
            try writer.print("\n", .{});
        },
        // warning print with colors
        .warn => {
            try writer.print("{s}[WARN]{s} ", .{ Colors.YELLOW, Colors.RESET });
            try writer.print(fmt, msg);
            try writer.print("\n", .{});
        },
        // error print with colors again
        .err => {
            try writer.print("{s}[ERR]{s} ", .{ Colors.RED, Colors.RESET });
            try writer.print(fmt, msg);
            try writer.print("\n", .{});
        },
    }
    errdefer {}
}

pub fn appPrint() !void {}
