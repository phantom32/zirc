const std = @import("std");
const input = @import("input.zig");

const Profile = @import("@root").Profile;

pub fn getConfig() !Profile {}
